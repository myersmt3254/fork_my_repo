//////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute links in multi-zone chemical evolution code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/assign.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/graph/iteration_macros.hpp>

//##############################################################################
// Check if already included.
//##############################################################################

#include "zone_linker/base/zone_linker_base.hpp"

#include <limits>

#include <boost/graph/iteration_macros.hpp>
#include <boost/tuple/tuple.hpp>

#ifndef NNP_ZONE_LINKER_DETAIL_HPP
#define NNP_ZONE_LINKER_DETAIL_HPP

namespace wn_user
{

namespace detail
{

//##############################################################################
// Strings.
//##############################################################################

#define S_SNII_DUST        "snII_dust"
#define S_SNIA_DUST        "snIa_dust"
#define S_LOW_MASS_DUST    "low_mass_dust"
#define S_LOW_ENTROPY_DUST "low_entropy_dust"
#define S_OLD_DUST         "old_dust"
#define S_GAS              "gas"
#define S_MC               "molecular_cloud"
#define S_GALAXY_MASS      "galaxy_mass"

class zone_linker : public zone_linker_base
{

  public:
    zone_linker() : zone_linker_base() {}
    zone_linker( v_map_t& v_map ) : zone_linker_base( v_map )
    {
      b_period = v_map[S_PERIOD].as<bool>();
      d_mix_rate = v_map[S_MIX_RATE].as<double>();
    }

//##############################################################################
// set_zones().
//##############################################################################

void
set_zones(
  std::vector<nnt::Zone>& zones,
  ice_data_t& ice_data
)
{
  Libnucnet * p_nucnet = boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] );

  gsl_vector * p_abunds =
    Libnucnet__Zone__getAbundances( zones[0].getNucnetZone() );

  std::vector<nnt::Zone> new_zones;

  std::vector<std::string> v_s_zone =
    boost::assign::list_of(S_HALO)(S_SNII_DUST)(S_SNIA_DUST)
                          (S_LOW_MASS_DUST)(S_LOW_ENTROPY_DUST)
                          (S_OLD_DUST)(S_GAS)(S_MC);

  BOOST_FOREACH( std::string s, v_s_zone )
  {
    nnt::Zone zone;
    zone.setNucnetZone(
      Libnucnet__Zone__new(
        Libnucnet__getNet( p_nucnet ),
        s.c_str(),
        "0",
        "0"
      )
    );
    if( s == std::string( S_HALO ) )
    {
      zone.updateProperty(
        nnt::s_ZONE_MASS,
        boost::any_cast<double>( ice_data[S_GALAXY_MASS] )
      );
    }
    else
    {
      zone.updateProperty(
        nnt::s_ZONE_MASS,
        "1.e-10"
      );
    }
    Libnucnet__Zone__updateAbundances( zone.getNucnetZone(), p_abunds );
    new_zones.push_back( zone );
  }

  gsl_vector_free( p_abunds );

  //==========================================================================
  // Add new zones to nucnet.
  //==========================================================================

  for( size_t i = 0; i < zones.size(); i++ )
  {
     Libnucnet__removeZone( p_nucnet, zones[i].getNucnetZone() );
  }

  zones.clear();

  BOOST_FOREACH( nnt::Zone new_zone, new_zones )
  {
    Libnucnet__addZone( p_nucnet, new_zone.getNucnetZone() );
    zones.push_back( new_zone );
  }

}

//##############################################################################
// get_injecta_zone().
//##############################################################################

nnt::Zone
get_injecta_zone(
  ice_data_t& ice_data,
  const StarSystem& star_system
)
{

  nnt::Zone zone;
  Star star = star_system.getTopStar();

  if( star.getStatus() == S_STAR )
  {
    if( star.getMass() < 10. )
    {
      zone.setNucnetZone(
        Libnucnet__getZoneByLabels(
          boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] ),
          S_LOW_MASS_DUST,
          "0",
          "0"
        )
      );
    }
    else
    {
      zone.setNucnetZone(
        Libnucnet__getZoneByLabels(
          boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] ),
          S_SNII_DUST,
          "0",
          "0"
        )
      );
    }
  }
  else if( star.getStatus() == S_WHITE_DWARF )
  {
    zone.setNucnetZone(
      Libnucnet__getZoneByLabels(
        boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] ),
        S_SNIA_DUST,
        "0",
        "0"
      )
    );
  }
  else if( star.getStatus() == S_C_WHITE_DWARF )
  {
    zone.setNucnetZone(
      Libnucnet__getZoneByLabels(
        boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] ),
        S_LOW_ENTROPY_DUST,
        "0",
        "0"
      )
    );
  }


  return zone;

}

//##############################################################################
// get_star_forming_zones().
//##############################################################################

std::vector<nnt::Zone>
get_star_forming_zones(
  Libnucnet * p_nucnet
)
{

  std::vector<nnt::Zone> zones;

  BOOST_FOREACH( nnt::Zone zone, nnt::make_zone_list( p_nucnet ) )
  {
    std::string s =
      std::string(Libnucnet__Zone__getLabel( zone.getNucnetZone(), 1 ) );
    if( s == S_MC )
    {
      zones.push_back( zone );
    }
  }

  return zones;

}

//##############################################################################
// set_links().
//##############################################################################

void
set_links( 
  user::zone_link_graph_t& g, 
  std::vector<nnt::Zone>& zones,
  ice_data_t& ice_data
)
{
  typedef user::zone_link_graph_t Graph;
  Graph::vertex_descriptor v_from, v_to;
  Graph::edge_descriptor e;
  user::vertex_multi_index vm;
  user::fill_multi_zone_vertex_hash( g, vm );

  bool e_add;

  // SNIA_DUST TO GAS

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_SNIA_DUST, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_GAS, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e8 * D_YEAR) );

  // SNIA_DUST TO MC

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_MC, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e7 * D_YEAR) );

  // LOW_ENTROPY_DUST TO GAS

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_LOW_ENTROPY_DUST, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_GAS, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e8 * D_YEAR) );

  // LOW_ENTROPY_DUST TO MC

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_MC, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e7 * D_YEAR) );

  // SNII_DUST TO GAS

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_SNII_DUST, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_GAS, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e8 * D_YEAR) );

  // SNII_DUST TO MC

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_MC, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e7 * D_YEAR) );

  // LOW_MASS_DUST TO GAS

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_LOW_MASS_DUST, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_GAS, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e8 * D_YEAR) );

  // LOW_MASS_DUST TO MC 

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_MC, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e7 * D_YEAR) );

  // GAS TO MC

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_GAS, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_MC, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e7 * D_YEAR) );

  // MC TO OLD DUST

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_MC, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_OLD_DUST, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e7 * D_YEAR) );

  // OLD DUST TO MC 

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_OLD_DUST, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_MC, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e7 * D_YEAR) );

  // OLD DUST TO GAS

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_OLD_DUST, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_GAS, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (2.e8 * D_YEAR) );

  // HALO TO GAS

  v_from =
    user::get_vertex_from_multi_zone_hash( vm, S_HALO, "0", "0" );

  v_to =
    user::get_vertex_from_multi_zone_hash( vm, S_GAS, "0", "0" );

  boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

  g[e].setWeight( 1. / (1.e9 * D_YEAR) );

}

//##############################################################################
// zone_linker_options().
//##############################################################################

class zone_linker_options : public zone_linker_base_options
{

  public:
    zone_linker_options() : zone_linker_base_options() {}

    void
    getDetailOptions( po::options_description& zone_linker )
    {

      try
      {

        zone_linker.add_options()

          ( S_PERIOD, po::value<bool>()->default_value( false, "false" ),
            "Periodic boundary conditions"
          )

          ( S_MIX_RATE, po::value<double>()->default_value( 0.01, "0.01" ),
            "Mixing rate between zones (s^-1)"
          )

        ;

        getBaseOptions( zone_linker );

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

    std::string
    getDetailExample()
    {
      return
        getBaseExample() +
        "--" + std::string( S_MIX_RATE ) + " 10 ";
    }

};

} // namespace detail

} // namespace wn_user

#endif  // ZONE_LINKER_DETAIL_HPP

