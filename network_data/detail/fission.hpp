////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

/// @cond _NETWORK_DATA_FISSION_

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_data/base/network_data.hpp"

#ifndef NNP_NETWORK_DATA_DETAIL_HPP
#define NNP_NETWORK_DATA_DETAIL_HPP

#define  S_A_FISSION         "a_fission"
#define  S_Z_DIST            "z_dist"
#define  S_N_DIST            "n_dist"
#define  S_SIGMA_Z           "sigma_z"
#define  S_SIGMA_N           "sigma_n"
#define  S_F_CUTOFF          "f_cutoff"
#define  S_FISSION_RATE      "fission_rate"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_data().
//##############################################################################

class network_data : public base::network_data
{

  public:
    network_data( v_map_t& v_map ) :
      base::network_data( v_map )
    {

      double d_rate, d_sigma_z, d_sigma_n;

      Libnucnet__Nuc * p_nuc =
        Libnucnet__Net__getNuc( Libnucnet__getNet( getNucnet() ) );

      Libnucnet__Reac * p_reac =
        Libnucnet__Net__getReac( Libnucnet__getNet( getNucnet() ) );

      d_rate = v_map[S_FISSION_RATE].as<double>();

      d_sigma_z = v_map[S_SIGMA_Z].as<double>();

      d_sigma_n = v_map[S_SIGMA_N].as<double>();

      BOOST_FOREACH( nnt::Species species1, nnt::make_species_list( p_nuc ) )
      {

        double z = Libnucnet__Species__getZ( species1.getNucnetSpecies() );

        double a = Libnucnet__Species__getA( species1.getNucnetSpecies() );

        double z0 =
          fabs( 0.5 - v_map[S_Z_DIST].as<double>() ) * z;

        double n0 =
          fabs( 0.5 - v_map[S_N_DIST].as<double>() ) *
          (a - z);

        if(
          (unsigned int) a >= v_map[S_A_FISSION].as<unsigned int>()
        )
        {

          BOOST_FOREACH(
            nnt::Species species2,
            nnt::make_species_list( p_nuc )
          )
          {

            double zf = Libnucnet__Species__getZ( species2.getNucnetSpecies() );

            double af = Libnucnet__Species__getA( species2.getNucnetSpecies() );

            double nf = af - zf;

            double f =
              ( 1. / ( 2. * M_PI * d_sigma_z * d_sigma_n ) )
              *
              (
                exp( -0.5 * gsl_pow_2( ( zf - z0 ) / d_sigma_z ) )
                *
                exp( -0.5 * gsl_pow_2( ( nf - n0 ) / d_sigma_n ) )
              );

            if( f > v_map[S_F_CUTOFF].as<double>() )
            {

              unsigned int i_z1 = zf;

              unsigned int i_z2 = (unsigned int) z - i_z1;

              unsigned int i_a1 = af;

              unsigned int i_a2 = (unsigned int) a - i_a1;

              Libnucnet__Species * pf1 =
                Libnucnet__Nuc__getSpeciesByZA( p_nuc, i_z1, i_a1, "" );

              Libnucnet__Species * pf2 =
                Libnucnet__Nuc__getSpeciesByZA( p_nuc, i_z2, i_a2, "" );

              if( pf1 && pf2 && i_z1 != 0 && i_z2 != 0 )
              {
                Libnucnet__Reaction * p_reaction = Libnucnet__Reaction__new();
                Libnucnet__Reaction__addReactant(
                  p_reaction,
                  Libnucnet__Species__getName( species1.getNucnetSpecies() )
                );
                Libnucnet__Reaction__addProduct(
                  p_reaction, Libnucnet__Species__getName( pf1 )
                );
                Libnucnet__Reaction__addProduct(
                  p_reaction, Libnucnet__Species__getName( pf2 )
                );
                Libnucnet__Reaction__updateSingleRate( p_reaction, d_rate * f );
                Libnucnet__Reac__addReaction( p_reac, p_reaction );
              }
            }
          }
        }
      }
    }

};
    
//##############################################################################
// network_data_options().
//##############################################################################

class network_data_options : base::network_data_options
{

  public:
    network_data_options() : base::network_data_options() {}

    std::string
    getExample()
    {
      return base::network_data_options::getExample();
    }

    void
    get( po::options_description& network_data )
    {

      try
      {

        network_data.add_options()

        // Option for fission rate
        (
          S_FISSION_RATE, po::value<double>()->default_value( 1., "1." ),
          "Fission rate (per second)"
        )

        // Option for fissioning a
        (
          S_A_FISSION, po::value<unsigned int>()->default_value( 280, "280" ),
          "Fissioning A (all nuclei with mass number >= A fission)"
        )

        // Option for distribution cutoff
        (
          S_F_CUTOFF, po::value<double>()->default_value( 1.e-10, "1.e-10" ),
          "Distribution cutoff"
        )

        // Option for z distribution
        (
          S_Z_DIST, po::value<double>()->default_value( 0., "0." ),
          "Z Asymmetry in binary fission fragments (0. = symmetric fragments)"
        )

        // Option for n distribution
        (
          S_N_DIST, po::value<double>()->default_value( 0., "0." ),
          "N Asymmetry in binary fission fragments (0. = symmetric fragments)"
        )

        // Option for spread in z
        (
          S_SIGMA_Z, po::value<double>()->default_value( 5, "5" ),
          "Z spread in fragment distribution"
        )

        // Option for spread in n
        (
          S_SIGMA_N, po::value<double>()->default_value( 5, "5" ),
          "N spread in fragment distribution"
        )
        ;

        base::network_data_options::get( network_data );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_NETWORK_DATA_DETAIL_HPP

/// @endcond
