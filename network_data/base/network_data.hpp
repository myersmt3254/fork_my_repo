////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file
//!
////////////////////////////////////////////////////////////////////////////////

#include "xml.hpp"

#ifndef NNP_NETWORK_DATA_BASE_HPP
#define NNP_NETWORK_DATA_BASE_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace network_data
{

namespace base
{

/// \var NUC_XPATH
/// \brief Option variable for value of XPath to select nuclei. 
struct
{
  const char * name="nuc_xpath";
  const char * default_value="";
  const char * comment="XPath to select nuclei (default: all nuclides)";
} NUC_XPATH;

/// \var REAC_XPATH
/// \brief Option variable for value of XPath to select reactions. 
struct
{
  const char * name="reac_xpath";
  const char * default_value="";
  const char * comment="XPath to select reactions (default: all reactions)";
} REAC_XPATH;

////////////////////////////////////////////////////////////////////////////////
///
/// \class network_data
///
/// \brief A class to handle common network data.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// network_data().
//##############################################################################

class network_data : public xml
{

  public:
/// The constructor.
/// \param The variable options map.

    network_data( v_map_t& v_map ) : xml( v_map )
    {
      s_nuc_xpath = v_map[NUC_XPATH.name].as<std::string>();
      s_reac_xpath = v_map[REAC_XPATH.name].as<std::string>();
    }

/// Routine to set the nuclear data from a data file.

    void
    setData()
    {
      Libnucnet__Net__updateFromXml(
        getNetwork(),
        getFileName().c_str(),
        s_nuc_xpath.c_str(),
        s_reac_xpath.c_str()
      );

      BOOST_FOREACH( std::string s, getExtraNucFiles() )
      {
        Libnucnet__Nuc__updateFromXml(
          Libnucnet__Net__getNuc(
            getNetwork()
          ),
          s.c_str(),
          s_nuc_xpath.c_str()
        );
      }

      BOOST_FOREACH( std::string s, getExtraReacFiles() )
      {
        Libnucnet__Reac__updateFromXml(
          Libnucnet__Net__getReac(
            getNetwork()
          ),
          s.c_str(),
          s_reac_xpath.c_str()
        );
      }
    }

/// Routine to retrieve the XPath expression for the nuclide data.
///
/// \return The XPath string.

    std::string
    getNucXPath() { return s_nuc_xpath; }

/// Routine to retrieve the XPath expression for the reaction data.
///
/// \return The XPath string.

    std::string
    getReacXPath() { return s_reac_xpath; }

  private:
    std::string s_nuc_xpath, s_reac_xpath;

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class options
///
/// \brief A class to handle common network data options.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// options().
//##############################################################################

class options : public xml_options
{

  public:
    options() : xml_options(){}

/// Routine to retrieve the base example string.
/// \return The example string.

    std::string
    getExample()
    {

      return
        xml_options::getExample() + " " +
        "--" + std::string( NUC_XPATH.name ) + " \"[z <= 30]\" ";

    }

/// Routine to update the base network data option descriptions.
///
/// \param options_desc The option descriptions.
///
/// \return On successful return, the option descriptions have been updated.

    void
    get( po::options_description& options_desc )
    {

      try
      {

        getXmlOptions( options_desc );

        options_desc.add_options()

        (
          NUC_XPATH.name,
          po::value<std::string>()->default_value( NUC_XPATH.default_value ),
          NUC_XPATH.comment
        )
    
        (
          REAC_XPATH.name,
          po::value<std::string>()->default_value( REAC_XPATH.default_value ),
          REAC_XPATH.comment
        )
    
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace network_data

} // namespace wn_user

#endif // NNP_NETWORK_DATA_BASE_HPP
