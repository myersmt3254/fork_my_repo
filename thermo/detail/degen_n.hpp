////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file thermo.hpp
//! \brief A file to define thermo routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include <boost/bind.hpp>

#include "thermo/base/thermo_base.hpp"

#ifndef NNP_THERMO_DETAIL_HPP
#define NNP_THERMO_DETAIL_HPP

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

namespace detail
{

class thermo
{

  public:

    thermo(){}

    void registerThermoFunctions( nnt::Zone& zone )
    {

      zone.updateFunction(
        nnt::char_cat( nnt::s_BARYON, nnt::s_ENTROPY_PER_NUCLEON ),
        static_cast<boost::function<double( nnt::Zone& )> >(
          computeBaryonEntropyPerNucleon
        ),
        "The baryon entropy per nucleon in units of Boltzmann's constant.",
        nnt::s_THERMO
      );

      zone.updateFunction(
        nnt::char_cat( nnt::s_BARYON, nnt::s_PRESSURE ),
        static_cast<boost::function<double( nnt::Zone& )> >(
          computeBaryonPressure
        ),
        "The baryon pressure in dynes / cm^2.",
        nnt::s_THERMO
      );

      zone.updateFunction(
        nnt::char_cat( nnt::s_BARYON, nnt::s_SPECIFIC_HEAT_PER_NUCLEON ),
        static_cast<boost::function<double( nnt::Zone& )> >(
          computeBaryonSpecificHeatPerNucleon
        ),
        "The baryon specific heat per nucleon in units of Boltzmann's constant.",
        nnt::s_THERMO
      );

      zone.updateFunction(
        nnt::char_cat( nnt::s_BARYON, nnt::s_DPDT ),
        static_cast<boost::function<double( nnt::Zone& )> >(
          computeBaryonDPDT
        ),
        "The baryon derivative of pressure with respect to temperature.",
        nnt::s_THERMO
      );

    }

    static Libstatmech__Fermion *
    createNeutron( nnt::Species& species )
    {
      return
        Libstatmech__Fermion__new(
          "n",
          WN_AMU_TO_MEV * Libnucnet__Species__getA( species.getNucnetSpecies() )
          +
          Libnucnet__Species__getMassExcess(
            species.getNucnetSpecies()
          ),
          2,
          0.
       );
    }

    static double
    computeBaryonEntropyPerNucleon( nnt::Zone& zone )
    {

      double d_result = 0;
      double d_n, d_rho, d_T;

      d_rho = zone.getProperty<double>( nnt::s_RHO );

      d_n = GSL_CONST_NUM_AVOGADRO * d_rho;

      d_T = zone.getProperty<double>( nnt::s_T9 ) * GSL_CONST_NUM_GIGA;

      BOOST_FOREACH(
        nnt::Species species,
        user::get_thermo_species_list( zone )
      )
      {

        if( 
   	  Libnucnet__Zone__getSpeciesAbundance(
	    zone.getNucnetZone(),
	    species.getNucnetSpecies()
	  ) > 1.e-30
        )
        {

          if(
            std::string(
              Libnucnet__Species__getName( species.getNucnetSpecies() ) 
             ) != "n"
          )
          {

            d_result +=
    	      (
	        2.5 *
	        Libnucnet__Zone__getSpeciesAbundance(
	          zone.getNucnetZone(),
	          species.getNucnetSpecies()
	        )
	      )
	      -
	      (
	        Libnucnet__Zone__getSpeciesAbundance(
	          zone.getNucnetZone(),
	          species.getNucnetSpecies()
	        )
	        *
	        log(
	          Libnucnet__Zone__getSpeciesAbundance(
	            zone.getNucnetZone(),
	            species.getNucnetSpecies()
	          ) /
	          Libnucnet__Species__computeQuantumAbundance(
	            species.getNucnetSpecies(),
	            zone.getProperty<double>( nnt::s_T9 ),
	            zone.getProperty<double>( nnt::s_RHO )
	          )
	        )
	      );

            }
            else
            { 

              Libstatmech__Fermion * p_neutron = createNeutron( species );

              double d_mukT =
                Libstatmech__Fermion__computeChemicalPotential(
                  p_neutron,
                  d_T,
                  Libnucnet__Zone__getSpeciesAbundance(
                    zone.getNucnetZone(),
                    species.getNucnetSpecies()
                  ) * d_n,
                  NULL,
                  NULL
                );

              d_result+=
                Libstatmech__Fermion__computeQuantity(
                  p_neutron,
                  S_ENTROPY_DENSITY,
                  d_T,
                  d_mukT,
                  NULL,
                  NULL
                ) / d_n;

              Libstatmech__Fermion__free( p_neutron );

          }
        }
      }

      return d_result;

    }

    static double
    computeBaryonPressure( nnt::Zone& zone )
    {

      double d_pB = 0;
      double d_n, d_rho, d_T;

      d_rho = zone.getProperty<double>( nnt::s_RHO );

      d_n = GSL_CONST_NUM_AVOGADRO * d_rho;

      d_T = zone.getProperty<double>( nnt::s_T9 ) * GSL_CONST_NUM_GIGA;

      BOOST_FOREACH(
        nnt::Species species,
        user::get_thermo_species_list( zone )
      )
      {
        if( 
   	  Libnucnet__Zone__getSpeciesAbundance(
	    zone.getNucnetZone(),
	    species.getNucnetSpecies()
	  ) > 1.e-30
        )
        {

          if(
            std::string(
              Libnucnet__Species__getName( species.getNucnetSpecies() ) 
            ) != "n"
          )
          {

            d_pB +=
              Libnucnet__Zone__getSpeciesAbundance(
                zone.getNucnetZone(),
                species.getNucnetSpecies()
              ) *
              d_n *
              GSL_CONST_CGSM_BOLTZMANN *
              d_T;

          }
          else
          { 

            Libstatmech__Fermion * p_neutron = createNeutron( species );

            double d_mukT =
              Libstatmech__Fermion__computeChemicalPotential(
                p_neutron,
                d_T,
                Libnucnet__Zone__getSpeciesAbundance(
                  zone.getNucnetZone(),
                  species.getNucnetSpecies()
                ) * d_n,
                NULL,
                NULL
              );

            d_pB +=
              Libstatmech__Fermion__computeQuantity(
                p_neutron,
                S_PRESSURE,
                d_T,
                d_mukT,
                NULL,
                NULL
              );

            Libstatmech__Fermion__free( p_neutron );

          }

        }

      }

      return d_pB;

    }

    static double
    computeBaryonSpecificHeatPerNucleon( nnt::Zone& zone )
    {

      double d_cvb = 0;

      BOOST_FOREACH(
        nnt::Species species,
        user::get_thermo_species_list( zone )
      )
      {

        if(
          std::string(
            Libnucnet__Species__getName( species.getNucnetSpecies() )
           ) != "n"
        )
        {
          d_cvb +=
            Libnucnet__Zone__getSpeciesAbundance(
              zone.getNucnetZone(),
              species.getNucnetSpecies()
            )
            *
            (
              1.5
              +
              (
                zone.getProperty<double>( nnt::s_T9 ) *
                GSL_CONST_NUM_GIGA *
                user::compute_dlnG_dT(
                  species.getNucnetSpecies(), 
                 zone.getProperty<double>( nnt::s_T9 ) * GSL_CONST_NUM_GIGA
                )
              )
           );
         }
         else
         {
           Libstatmech__Fermion * p_neutron = createNeutron( species );

           Libstatmech__Fermion__updateQuantityIntegralAccuracy(
             p_neutron,
             S_ENTROPY_DENSITY,
             0.,
             nnt::d_REL_EPS
           );

           double d_T =
             zone.getProperty<double>( nnt::s_T9 ) * GSL_CONST_NUM_GIGA;

           double d_nn =
             zone.getProperty<double>( nnt::s_RHO ) *
             GSL_CONST_NUM_AVOGADRO *
             Libnucnet__Zone__getSpeciesAbundance(
               zone.getNucnetZone(),
               species.getNucnetSpecies()
             );

           d_cvb +=
             d_T *
             Libstatmech__Fermion__computeTemperatureDerivative(
               p_neutron,
               S_ENTROPY_DENSITY,
               d_T,
               d_nn,
               NULL,
               NULL
            ) /
            (
              zone.getProperty<double>( nnt::s_RHO ) *
              GSL_CONST_NUM_AVOGADRO *
              GSL_CONST_CGSM_BOLTZMANN
            );

          Libstatmech__Fermion__free( p_neutron );
 
        }

      }

      return d_cvb;

    }
  
    static double
    computeBaryonDPDT( nnt::Zone& zone )
    {

      double d_dPdT = 0;
      double d_n, d_rho, d_T;

      d_rho = zone.getProperty<double>( nnt::s_RHO );

      d_n = GSL_CONST_NUM_AVOGADRO * d_rho;

      d_T = zone.getProperty<double>( nnt::s_T9 ) * GSL_CONST_NUM_GIGA;

      BOOST_FOREACH(
        nnt::Species species,
        user::get_thermo_species_list( zone )
      )
      {
        if( 
   	  Libnucnet__Zone__getSpeciesAbundance(
	    zone.getNucnetZone(),
	    species.getNucnetSpecies()
	  ) > 1.e-30
        )
        {

          if(
            std::string(
              Libnucnet__Species__getName( species.getNucnetSpecies() ) 
            ) != "n"
          )
          {

            d_dPdT +=
              Libnucnet__Zone__getSpeciesAbundance(
                zone.getNucnetZone(),
                species.getNucnetSpecies()
              ) *
              d_n *
              GSL_CONST_CGSM_BOLTZMANN;

          }
          else
          { 

            Libstatmech__Fermion * p_neutron = createNeutron( species );

            double d_nn =
              zone.getProperty<double>( nnt::s_RHO ) *
              GSL_CONST_NUM_AVOGADRO *
              Libnucnet__Zone__getSpeciesAbundance(
                zone.getNucnetZone(),
                species.getNucnetSpecies()
              );

            Libstatmech__Fermion__updateQuantityIntegralAccuracy(
              p_neutron,
              S_PRESSURE,
              0.,
              nnt::d_REL_EPS
            );

            d_dPdT +=
             Libstatmech__Fermion__computeTemperatureDerivative(
               p_neutron,
               S_PRESSURE,
               d_T,
               d_nn,
               NULL,
               NULL
            );

            Libstatmech__Fermion__free( p_neutron );

          }

        }

      }

      return d_dPdT;

    }


};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_THERMO_DETAIL_HPP
