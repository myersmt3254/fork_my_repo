////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file thermo.hpp
//! \brief A file to define thermo routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#ifndef NNP_THERMO_HPP
#define NNP_THERMO_HPP

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

class thermo : public detail::thermo
{

  public:

    thermo() : detail::thermo() {}

};

}  // namespace wn_user

#endif // NNP_THERMO_HPP
