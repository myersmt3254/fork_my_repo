////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file limiter.hpp
//! \brief A file to define time step routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef NNP_TIME_ADJUSTER_HPP
#define NNP_TIME_ADJUSTER_HPP

namespace wn_user
{

namespace time_adjuster
{

//##############################################################################
// time_adjuster().
//##############################################################################

class time_adjuster : public detail::time_adjuster
{

  public:
    time_adjuster( v_map_t& v_map ) : detail::time_adjuster( v_map ){}

};
     
//##############################################################################
// options().
//##############################################################################

class options : public detail::options
{

  public:
    options() : detail::options() {}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description options_desc("\nTimestep adjuster options");

        detail::options::get( options_desc );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "time_adjuster",
            options_struct( options_desc )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace time_adjuster

} // namespace wn_user 

#endif // NNP_TIME_ADJUSTER_HPP
