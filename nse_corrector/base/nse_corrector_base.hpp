// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file screening.hpp
//! \brief A file to define nse correction routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef NNP_NSE_CORRECTOR_BASE_HPP
#define NNP_NSE_CORRECTOR_BASE_HPP

#define S_NSE_CORR        "nse_corr"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// nse_corrector_base().
//##############################################################################

class nse_corrector_base
{

  public:
    nse_corrector_base(){}
    nse_corrector_base( v_map_t& v_map )
    {
      b_corr = v_map[S_NSE_CORR].as<bool>();
    }

    bool useNseCorrector() { return b_corr; }

  private:
    bool b_corr;

};

//##############################################################################
// nse_corrector_base_options().
//##############################################################################

class nse_corrector_base_options
{

  public:
    nse_corrector_base_options(){}

    void
    getBaseOptions( po::options_description& nse_corrector )
    {

      try
      {

        nse_corrector.add_options()

          ( S_NSE_CORR,
            po::value<bool>()->default_value( false, "false" ),
            "Use nse corrector" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }
};

}  // namespace detail

}  // namespace wn_user

#endif  // NNP_NSE_CORRECTOR_BASE_HPP
