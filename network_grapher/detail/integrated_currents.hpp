///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file integrated_currents.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_output_file.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#ifndef NNP_NETWORK_GRAPHER_DETAIL_HPP
#define NNP_NETWORK_GRAPHER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define   S_MAX_CURRENT_SCALE         "max_current_scale"

typedef std::map<std::string, double> currents_map_t;

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

//##############################################################################
// vertexWriter().
//##############################################################################

template<typename Graph>
class
vertexWriter : public base::vertexWriter<Graph>
{
  public:
    vertexWriter() : base::vertexWriter<Graph>(){}
    vertexWriter( Graph& g ) : base::vertexWriter<Graph>( g ){}

    template <class Vertex>
    void operator()( std::ostream& out, Vertex v )
    {
      base::vertexWriter<Graph>::write( out, v );
    }

};

//##############################################################################
// edgeWriter().
//##############################################################################

template<typename Graph>
class
edgeWriter : public base::edgeWriter<Graph>
{
  public:
    edgeWriter() : base::edgeWriter<Graph>(){}
    edgeWriter( Graph& g ) : base::edgeWriter<Graph>( g ){}

    template <class Edge>
    void operator()( std::ostream& out, Edge e )
    {
      base::edgeWriter<Graph>::write( out, e );
    }

};

//##############################################################################
// graphWriter().
//##############################################################################

template<typename Graph>
class
graphWriter : public base::graphWriter<Graph>
{
  public:
    graphWriter() : base::graphWriter<Graph>(){}
    graphWriter( Graph& g ) : base::graphWriter<Graph>( g ){};

    void operator()( std::ostream& out ) const
    {
      base::graphWriter<Graph>::write( out );
    }
};

//##############################################################################
// get_currents().
//##############################################################################

void
get_currents(
  const char * s_name,
  const char * s_tag1,
  const char * s_tag2,
  const char * s_value,
  currents_map_t * p_currents
)
{
  (*p_currents)[s_tag1] = boost::lexical_cast<double>( s_value );
}

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher :
  public base::network_grapher,
    public base::network_grapher_output_file,
    public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_output_file(),
      base::network_grapher_induced_view(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_output_file( v_map ),
      base::network_grapher_induced_view( v_map )
      {
        dMaxScale = v_map[S_MAX_CURRENT_SCALE].as<double>();
      }

    void
    write( std::ostream& my_out )
    {
      views_t views = getInducedViews( getGraphNetwork( g ) );
      boost::filtered_graph<graph_t, base::edgeFilter, base::vertexFilter>
        fg(
          g,
          base::edgeFilter( g, views.first, views.second ),
          base::vertexFilter( g, views.first, allowIsolatedVertices() )
        );

      scaleEdgeWeights( fg );

      boost::write_graphviz(
        my_out,
        fg,
        vertexWriter<filtered_graph_t>( fg ),
        edgeWriter<filtered_graph_t>( fg ),
        graphWriter<graph_t>( g )
      );
      clearViews( views );
    }

    void
    write()
    {
      std::ofstream my_out;
      my_out.open( getOutputFile().c_str() ),
      write( my_out );
      my_out.close();
    }

    void operator()( nnt::Zone& zone )
    {
      currents_map_t my_currents;
      Libnucnet__Zone * p_zone = zone.getNucnetZone();
      Libnucnet__Reac * p_reac =
        Libnucnet__Net__getReac( Libnucnet__Zone__getNet( p_zone ) );

      g = createGraph( Libnucnet__Zone__getNet( p_zone ) );

      Libnucnet__Zone__iterateOptionalProperties(
        p_zone,
        nnt::s_FLOW_CURRENT,
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function) get_currents,
        &my_currents
      );
       
      BOOST_FOREACH( currents_map_t::value_type& t, my_currents )
      {
        Libnucnet__Reaction * p_reaction =
          Libnucnet__Reac__getReactionByString( p_reac, t.first.c_str() );
        std::map<std::string, std::string> props;
        props["style"] = "solid";
        nnt::reaction_element_list_t reactants =
          nnt::make_reaction_nuclide_reactant_list( p_reaction );
        nnt::reaction_element_list_t products =
          nnt::make_reaction_nuclide_product_list( p_reaction );
        if( t.second >= 0 )
        {
          addReactionEdges(
            g, p_reaction, reactants, products, props, t.second
          );
        }
        else
        {
          addReactionEdges(
            g, p_reaction, products, reactants, props, t.second
          );
        }
      }
    }
        
  private:
    graph_t g;
    typedef
      boost::filtered_graph<graph_t, base::edgeFilter, base::vertexFilter>
      filtered_graph_t;
    double dMaxScale;

    template<class Graph>
    void
    scaleEdgeWeights( Graph& g )
    {
      double d_max = 0;
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        double d_weight = boost::get( boost::edge_weight, g )[e];
        if( fabs( d_weight ) > d_max ) { d_max = d_weight; }
      }
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        boost::get( boost::edge_weight, g )[e] *= dMaxScale / d_max;
      }
    }



};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
  public base::network_grapher_options,
  public base::network_grapher_output_file_options,
  public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_output_file_options(),
      base::network_grapher_induced_view_options(){}


    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString() +
        base::network_grapher_output_file_options::getExampleString() +
        base::network_grapher_induced_view_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Maximum current scale.
        (
          S_MAX_CURRENT_SCALE,
          po::value<double>()->default_value( 10., "10" ),
          "Maximum current scaling."
        )

        ;

        base::network_grapher_options::getOptions( network_grapher );

        base::network_grapher_output_file_options::getOptions(
          network_grapher
        );

        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_NETWORK_GRAPHER_DETAIL_HPP
