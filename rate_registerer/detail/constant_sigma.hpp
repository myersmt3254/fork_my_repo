////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file rate_registerer.hpp
//! \brief A file to define rate registration routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <map>
#include <string>

#include <boost/lexical_cast.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/math.h"
#include "user/network_utilities.h"

#include "rate_registerer/base/rate_registerer_base.hpp"

#ifndef NNP_CONSTANT_SIGMA_REGISTERER_HPP
#define NNP_CONSTANT_SIGMA_REGISTERER_HPP

#define S_CONSTANT_SIGMA "constant sigma"
#define S_SIGMA          "sigma"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// compute_constant_sigma_rate()
//##############################################################################

double
compute_constant_sigma_rate(
  Libnucnet__Reaction *p_reaction,
  double d_t9,
  nnt::Zone& zone
)
{

  double d_sigma =
    boost::lexical_cast<double>(
      Libnucnet__Reaction__getUserRateFunctionProperty(
        p_reaction,
        S_SIGMA,
        NULL,
        NULL
      )
    );

  return
    GSL_CONST_NUM_AVOGADRO * 
      d_sigma * GSL_CONST_CGSM_BARN * GSL_CONST_NUM_MILLI *
      user::compute_v_Thermal(
        Libnucnet__Nuc__getSpeciesByName(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          ),
          "n"
        ),
        zone.getProperty<double>( nnt::s_T9 )
      );

}

//##############################################################################
// constant_sigma_registerer().
//##############################################################################

class constant_sigma_registerer : public rate_registerer_base
{

  public:
    constant_sigma_registerer( v_map_t& v_map ) : rate_registerer_base(){}

    void operator()( nnt::Zone& zone )
    {

      Libnucnet__Reac * p_reac =
        Libnucnet__Net__getReac(
          Libnucnet__Zone__getNet( zone.getNucnetZone() )
        );

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        S_CONSTANT_SIGMA,
        (Libnucnet__Reaction__userRateFunction) compute_constant_sigma_rate
      );

      Libnucnet__Zone__updateDataForUserRateFunction(
        zone.getNucnetZone(),
        S_CONSTANT_SIGMA,
        &zone
      );

    }

    void operator()( std::vector<nnt::Zone>& zones )
    {

      Libnucnet__Reac * p_reac =
        Libnucnet__Net__getReac(
          Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
        );

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        S_CONSTANT_SIGMA,
        (Libnucnet__Reaction__userRateFunction) compute_constant_sigma_rate
      );

      BOOST_FOREACH( nnt::Zone& zone, zones )
      {
        Libnucnet__Zone__updateDataForUserRateFunction(
          zone.getNucnetZone(),
          S_CONSTANT_SIGMA,
          &zone
        );
      }

    }

};

//##############################################################################
// constant_sigma_registerer_options().
//##############################################################################

class constant_sigma_registerer_options : public rate_registerer_options_base
{

  public:
    constant_sigma_registerer_options() : rate_registerer_options_base(){}

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_CONSTANT_SIGMA_REGISTERER_HPP
