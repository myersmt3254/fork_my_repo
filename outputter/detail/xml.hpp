////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "outputter/base/xml.hpp"
#include "outputter/base/outputter.hpp"
#include "outputter/base/every_step.hpp"

#ifndef NNP_OUTPUTTER_XML_HPP
#define NNP_OUTPUTTER_XML_HPP

#define S_XML_STEPS    "xml_steps"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// outputter_xml().
//##############################################################################

class outputter_xml : public base::outputter,
                      public base::outputter_xml,
                      public base::outputter_every_step
{

  public:
    outputter_xml( v_map_t& v_map ) :
      base::outputter( v_map ),
      base::outputter_xml( v_map ),
      base::outputter_every_step( v_map )
    {
      iCount = 0;
      iXmlSteps = v_map[S_XML_STEPS].as<size_t>();
    }

    void set( Libnucnet * p_nucnet )
    {
      base::outputter_xml::set( p_nucnet );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      BOOST_FOREACH( nnt::Zone zone, zones )
      {
        base::outputter_xml::operator()( zones );
      }
    }

    void operator()( nnt::Zone& zone )
    {
      if( !getXmlFile().empty() )
      {
        if(
           (
             iCount++ % iXmlSteps == 0 ||
             zone.getProperty<double>( nnt::s_TIME ) >=
               zone.getProperty<double>( nnt::s_T_END )
           )
        )
        {
          base::outputter_xml::operator()( zone );
          if( outputEveryStep() )
          {
            write();
          }
        }
      }
    }

    void write()
    {
      if( !getXmlFile().empty() )
      {
        prepOutput1();
        Libnucnet__updateZoneXmlMassFractionFormat(
          getOutputNucnet(),
          getXmlFormat().c_str()
        );

        Libnucnet__writeToXmlFile(
          getOutputNucnet(),
          getXmlFile().c_str()
        );
        prepOutput2();
      }
    }

  private:
    size_t iXmlSteps, iCount;

};

//##############################################################################
// outputter_xml_options()
//##############################################################################

class outputter_xml_options : public base::outputter_xml_options,
                              public base::outputter_every_step_options,
                              public base::outputter_options
{

  public:
    outputter_xml_options() : base::outputter_xml_options(),
                              base::outputter_every_step_options(),
                              base::outputter_options() {}

    std::string
    getExample()
    {
      return
        base::outputter_options::getExample() +
        base::outputter_xml_options::getExample();
    }

    void operator()( po::options_description& outputter )
    {

      try
      {

        po::options_description xml("  Xml options");

        xml.add_options()

        ( S_XML_STEPS, po::value<size_t>()->default_value( 20, "20" ),
          "Frequency of output to xml file." )

        ; 

        base::outputter_xml_options::getOptions( xml );
        base::outputter_every_step_options::getOptions( xml );

        outputter.add( xml );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_OUTPUTTER_XML_HPP
