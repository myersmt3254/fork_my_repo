////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file exponential_rho.hpp
//! \brief A file to define useful hydro helper routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include "my_global_types.h"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

#define S_RHO_1    "rho_1"

#define S_TAU_0    "tau_0"
#define S_TAU_1    "tau_1"

#define HYDRO_EVOLVE

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

class hydro_detail : public thermo
{

  public:

    hydro_detail(){}
    hydro_detail( v_map_t& v_map ) : thermo()
    {
      d_t9_0 = v_map[nnt::s_T9_0].as<double>();
      d_rho_0 = v_map[nnt::s_RHO_0].as<double>();
      d_rho_1 = v_map[S_RHO_1].as<double>();
      d_tau_0 = v_map[S_TAU_0].as<double>();
      d_tau_1 = v_map[S_TAU_1].as<double>();
    }

    state_type
    set( nnt::Zone& zone )
    {
      state_type x;
      x.push_back( 1 );
      x.push_back(
        ( 1./3. ) * ( 1. / ( d_rho_0 + d_rho_1 ) ) *
        ( d_rho_0 / d_tau_0 + 2. * d_rho_1 / d_tau_1 )
      );
      x.push_back( d_t9_0 );
      return x;
    }

    void initialize( const state_type& x, nnt::Zone& zone ){}

    double
    computeHeatingRate(
      const state_type& x,
      double d_t,
      nnt::Zone& zone,
      Libnucnet__NetView * p_view
    )
    {
      return -computeEnergyLossRate( zone, p_view );
    }

    double computeRho( const state_type& x, double d_t, nnt::Zone& zone )
    {
      return d_rho_0 * exp( -d_t / d_tau_0 ) +
             d_rho_1 / gsl_pow_2( 1. + d_t / d_tau_1 );
    }

    double computeDlnRhoDt(
      const state_type& x, const double d_t, nnt::Zone& zone
    )
    {
      return
        (
          -( d_rho_0 / d_tau_0 ) * exp( -d_t / d_tau_0 )
          -2. * ( d_rho_1 / d_tau_1 ) / gsl_pow_3( 1. + d_t / d_tau_1 )
        ) / computeRho( x, d_t, zone );
    }

    double
    computeAcceleration(
      const state_type& x, const double d_t, nnt::Zone& zone
    )
    {
      return
        ( 4. / 9. ) * x[0] * gsl_pow_2( computeDlnRhoDt( x, d_t, zone ) )
        -
        ( 1. / 3. ) * ( x[0] / computeRho( x, d_t, zone ) )
        *
        (
          ( d_rho_0 / gsl_pow_2( d_tau_0 ) ) * exp( -d_t / d_tau_0 )
          +
          6. * ( d_rho_1 / gsl_pow_2( d_tau_1 ) )
          /
          gsl_pow_4( 1. + d_t / d_tau_1 )
        );
    }

    void
    updateOtherZoneProperties(const state_type& x, nnt::Zone& zone ) { }

  private:
    double d_tau_0, d_tau_1, d_t9_0, d_rho_0, d_rho_1, d_rho_i;

};

//##############################################################################
// hydro_detail_options().
//##############################################################################

class hydro_detail_options
{

  public:
    hydro_detail_options(){}

    void
    getDetailOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( nnt::s_T9_0, po::value<double>()->default_value( 10., "10." ),
            "Initial T (in 10^9 K)"
          )

          ( nnt::s_RHO_0,
            po::value<double>()->default_value( 1.4985e6, "1.4985e6" ),
            "Initial density 0 (in g/cc)"
          )

          ( S_RHO_1, po::value<double>()->default_value( 1.5e3, "1.5e3" ),
            "Initial density 1 (in g/cc)"
          )

          ( S_TAU_0, po::value<double>()->default_value( 0.035, "0.035" ),
            "Expansion timescale 0 (s)"
          )

          ( S_TAU_1, po::value<double>()->default_value( 1., "1." ),
            "Expansion timescale 1 (s)"
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

    std::string
    getExample()
    {

      return
        "--" + std::string( nnt::s_T9_0 ) + " 10 " +
        "--" + std::string( nnt::s_TAU ) + " 0.1 ";

    }

};

}  // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
