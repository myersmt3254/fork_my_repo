////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file hydro.hpp
//! \brief A file to define hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "user/network_utilities.h"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// hydro_detail().
//##############################################################################

class hydro_detail
{

  public:
    hydro_detail( v_map_t& v_map ){}

    void
    storeDataFromFile( std::string s_file )
    {
      std::ifstream my_file;
      double d_x1, d_x2, d_x3, d_x4, d_x5, d_x6;

      my_file.open( s_file.c_str() );

      if( !my_file.is_open() || my_file.bad() )
      {
        std::cerr << "Couldn't open file " << s_file << "!" << std::endl;
        exit( EXIT_FAILURE );
      }

      while( my_file >> d_x1 >> d_x2 >> d_x3 >> d_x4 >> d_x5 >> d_x6 )
      {
        time.push_back( d_x1 );
        t9.push_back( d_x2 );
        log10_rho.push_back( log10( d_x3 ) );
      }

      my_file.close();

    }

    std::vector<double> getTimeVector() const { return time; }

    std::vector<double> getT9Vector() const { return t9; }

    std::vector<double> getLog10RhoVector() const { return log10_rho; }

    void
    adjustTimeStep( nnt::Zone& zone ) {}

  private:
    std::vector<double> time, t9, log10_rho;

};
 
//##############################################################################
// hydro_detail_options().
//##############################################################################

class hydro_detail_options
{

  public:
    hydro_detail_options(){}

    void
    getDetailOptions( po::options_description& hydro ){}

};

} // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
