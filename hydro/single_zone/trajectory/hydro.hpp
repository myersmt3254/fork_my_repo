////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file hydro.hpp
//! \brief A file to define hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "user/network_utilities.h"

#ifndef NNP_HYDRO_HPP
#define NNP_HYDRO_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// hydro().
//##############################################################################

class hydro : public detail::hydro
{

  public:
    hydro( v_map_t& v_map ) : detail::hydro( v_map ){}

};
 
//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options : public detail::hydro_options
{

  public:
    hydro_options() : detail::hydro_options(){}

    std::string
    getExample()
    {
      return detail::hydro_options::getExample();
    }

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description hydro("\nHydro options");

        detail::hydro_options::getOptions( hydro );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "hydro",
            options_struct( hydro, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace wn_user

#endif // NNP_HYDRO_HPP
