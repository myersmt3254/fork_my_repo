////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file hydro.hpp
//! \brief A file to define hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/numeric/odeint.hpp>

#include "my_global_types.h"

#include "nnt/iter.h"
#include "hydro/single_zone/base/hydro.hpp"
#include "hydro/single_zone/base/hydro_stepper.hpp"

#ifndef NNP_HYDRO_HPP
#define NNP_HYDRO_HPP

#define S_OBSERVE          "observe"
#define S_SDOT_NUC_XPATH   "sdot_nuc_xpath"
#define S_SDOT_REAC_XPATH  "sdot_reac_xpath"
#define S_X_FAC            "x_factor"
#define S_X_LIM            "x_lim"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

void
observe(
  nnt::Zone& zone,
  const state_type& x,
  const state_type& dxdt,
  const double d_t
)
{

  double d_dt =
    d_t - zone.getProperty<double>( nnt::s_TIME );

  std::cout <<
    boost::format( "t = %.5e dt = %.5e\n" ) %
    d_t %
    d_dt;

  std::cout <<
    boost::format( "x = {%.5e, %.5e, %.5e}\n" ) %
    x[0] %
    x[1] %
    x[2]; 

  std::cout <<
    boost::format( "dxdt = {%.5e, %.5e, %.5e}\n\n" ) %
    dxdt[0] %
    dxdt[1] %
    dxdt[2];

}

struct
entropy_rhs
{

  nnt::Zone& zone;
  network_evolver& my_evolver;
  detail::hydro& my_hydro;
  Libnucnet__NetView * p_view;
  double dT9dt;
  bool b_observe;

  entropy_rhs(
    nnt::Zone& _zone,
    network_evolver& _my_evolver,
    detail::hydro& _my_hydro,
    Libnucnet__NetView * _p_view,
    double _dT9dt,
    bool _b_observe
  ) : zone( _zone ), my_evolver( _my_evolver ), my_hydro( _my_hydro ),
      p_view( _p_view ), dT9dt( _dT9dt ), b_observe( _b_observe ){}

  void
  operator()( const state_type &x, state_type &dxdt, const double d_t ) const
  {

    double d_dt, d_t9_old, d_dt_old;
    gsl_vector * p_abundances, * p_abundance_changes;

    p_abundances = Libnucnet__Zone__getAbundances( zone.getNucnetZone() );

    p_abundance_changes =
      Libnucnet__Zone__getAbundanceChanges( zone.getNucnetZone() );

    d_dt_old = zone.getProperty<double>( nnt::s_DTIME );

    d_t9_old = zone.getProperty<double>( nnt::s_T9 );

    d_dt =
      d_t 
      -
      zone.getProperty<double>( nnt::s_TIME );

    zone.updateProperty(
      nnt::s_DTIME,
      d_dt
    );

    zone.updateProperty( nnt::s_ENTROPY_PER_NUCLEON, x[2] );

    if( d_dt > 0 )
    {
      zone.updateProperty(
        nnt::s_RHO,
        my_hydro.computeRho( x, d_t, zone )
      );

      double d_t9_guess =
        zone.getProperty<double>( nnt::s_T9 ) + dT9dt * d_dt;

      zone.updateProperty(
        nnt::s_T9,
        my_hydro.computeT9FromEntropy(
          zone,
          Libnucnet__Zone__getEvolutionNetView( zone.getNucnetZone() ),
          d_t9_guess
        )
      );

      my_evolver( zone );
    }

    dxdt[0] = x[1];

    dxdt[1] = my_hydro.computeAcceleration( x, d_t, zone );

    dxdt[2] =
      my_hydro.computeEntropyGenerationRate( zone, p_view ) -
      my_hydro.computeEntropyLossRate( zone, p_view ) +
      my_hydro.computeHeatingRatePerNucleon( x, d_t, zone ) /
        ( GSL_CONST_CGSM_BOLTZMANN * GSL_CONST_NUM_GIGA *
          zone.getProperty<double>( nnt::s_T9 )
        );

    if( b_observe )
    {
      observe( zone, x, dxdt, d_t );
    }

    Libnucnet__Zone__updateAbundances( zone.getNucnetZone(), p_abundances );
  
    Libnucnet__Zone__updateAbundanceChanges(
      zone.getNucnetZone(),
      p_abundance_changes
    );

    zone.updateProperty( nnt::s_T9, d_t9_old );

    zone.updateProperty( nnt::s_DTIME, d_dt_old );

  }

};

//##############################################################################
// do_step().
//##############################################################################

struct do_step : public boost::static_visitor<>
{
  entropy_rhs rhs;
  state_type& x;
  double d_t, d_dt;
  do_step( entropy_rhs& _rhs, state_type& _x, double _d_t, double _d_dt ) :
    rhs( _rhs ), x( _x ), d_t( _d_t ), d_dt( _d_dt ) {}

  template <typename T>
  void operator()(T& t) const { t.do_step( rhs, x, d_t, d_dt ); }
};

//##############################################################################
// hydro().
//##############################################################################

class hydro : public base::hydro, public base::hydro_stepper
{

  public:
    hydro( v_map_t& v_map ) :
      base::hydro( v_map ), hydro_stepper( v_map ), my_evolver( v_map ),
      my_hydro( v_map )
    {
      if( v_map.count( S_SDOT_NUC_XPATH ) )
      {
        s_sdot_nuc_xpath = v_map[S_SDOT_NUC_XPATH].as<std::string>();
      }
      if( v_map.count( S_SDOT_REAC_XPATH ) )
      {
        s_sdot_nuc_xpath = v_map[S_SDOT_REAC_XPATH].as<std::string>();
      }
      b_observe = v_map[S_OBSERVE].as<bool>();
      d_x_lim = v_map[S_X_LIM].as<double>();
      d_x_fac = v_map[S_X_FAC].as<double>();
      stepper = getStepper();
      dT9dt = 0;
    }

    void setAbundancesToEquilibrium( nnt::Zone& zone )
    {
      zone.updateProperty(
        nnt::s_YE,
        user::compute_cluster_abundance_moment( zone, "", "z", 1 )
      );
      nnt::set_zone_abundances_to_equilibrium( zone );
    }

    void
    initialize( nnt::Zone& zone )
    {
      x = my_hydro.initializeX( zone );
      my_hydro.setT9andRho( x, zone );
      if( initializeEquilibrium() ) { setAbundancesToEquilibrium( zone ); }
      my_hydro.initialize( x, zone );
      std::copy( x.begin(), x.end(), std::back_inserter( x0 ) );
      my_evolver.initializeForZone( zone );
    }

    void
    operator()( nnt::Zone& zone )
    {

      Libnucnet__NetView * p_view;

      double d_t_s = zone.getProperty<double>( nnt::s_TIME );
      double d_dt = zone.getProperty<double>( nnt::s_DTIME );
      double d_t = d_t_s - d_dt;
      zone.updateProperty( nnt::s_TIME, d_t );
      double d_T9_old = zone.getProperty<double>( nnt::s_T9 );

      std::copy( x.begin(), x.end(), x0.begin() );

      if( s_sdot_nuc_xpath.empty() && s_sdot_reac_xpath.empty() )
      {
        p_view = Libnucnet__Zone__getEvolutionNetView( zone.getNucnetZone() );
      }
      else
      {
        if( s_sdot_nuc_xpath.empty() )
        {
          p_view = zone.getNetView( "", s_sdot_reac_xpath.c_str() );
        }
        if( s_sdot_reac_xpath.empty() )
        {
          p_view = zone.getNetView( s_sdot_nuc_xpath.c_str(), "" );
        }
        else
        {
          p_view =
            zone.getNetView(
              s_sdot_nuc_xpath.c_str(), s_sdot_reac_xpath.c_str()
            );
        }
      }

      entropy_rhs
        my_rhs( zone, my_evolver, my_hydro, p_view, dT9dt, b_observe );

      boost::apply_visitor( do_step( my_rhs, x, d_t, d_dt ), stepper );

      zone.updateProperty( nnt::s_RHO, my_hydro.computeRho( x, d_t, zone ) );

      double d_t9_guess =
        zone.getProperty<double>( nnt::s_T9 )
        +
        dT9dt * d_dt;

      zone.updateProperty(
        nnt::s_ENTROPY_PER_NUCLEON,
        x[2]
      );

      zone.updateProperty(
        nnt::s_T9,
        my_hydro.computeT9FromEntropy(
          zone,
          Libnucnet__Zone__getEvolutionNetView( zone.getNucnetZone() ),
          d_t9_guess
        )
      );

      zone.updateProperty( nnt::s_TIME, d_t_s );

      my_hydro.updateOtherZoneProperties( x, zone );

      if( b_observe )
      {
        std::cout <<
          boost::format( "t = %g, x = {%g, %g, %g}\n\n" ) %
          d_t_s % x[0] % x[1] % x[2];
        std::cout << boost::format( "-----------\n\n" );
      }

      dT9dt = (zone.getProperty<double>( nnt::s_T9 ) - d_T9_old ) / d_dt;

    }

    void
    adjustTimeStep( nnt::Zone& zone )
    {
      double d_dt = zone.getProperty<double>( nnt::s_DTIME );
      for( size_t i = 0; i < x.size(); i++ )
      {
        double dxdt = ( x[i] - x0[i] ) / d_dt;
        if( fabs( x[i] ) > d_x_lim && fabs( dxdt ) > 0 )
        {
          double d_dtm = d_x_fac * fabs( x[i] ) / fabs( dxdt );
          if( d_dtm < d_dt ) { d_dt = d_dtm; }
        }
      }
      zone.updateProperty( nnt::s_DTIME, d_dt );
    }

  private:
    network_evolver my_evolver;
    detail::hydro my_hydro;
    bool b_observe;
    std::string s_sdot_nuc_xpath, s_sdot_reac_xpath;
    base::Stepper stepper;
    state_type x, x0;
    double d_x_fac, d_x_lim, dT9dt;

};
 
//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options :
  public detail::hydro_options, public base::hydro_stepper_options
{

  public:
    hydro_options() :
      detail::hydro_options(), base::hydro_stepper_options() {}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description hydro("\nHydro options");
        hydro.add_options()

          ( S_SDOT_NUC_XPATH,
            po::value<std::string>(),
            "Entropy generation nuclear XPath (default: the evolution nuclides)"
          )

          ( S_SDOT_REAC_XPATH,
            po::value<std::string>(),
            "Entropy generation nuclear XPath (default: the evolution reactions)"
          )

          (
            S_OBSERVE,
            po::value<bool>()->default_value( false, "false" ),
            "Observe steps"
          )

          ( S_X_FAC, po::value<double>()->default_value( 0.1, "0.1" ),
            "Hydro timescale factor"
          )

          ( S_X_LIM, po::value<double>()->default_value( 1.e-10, "1.e-10" ),
            "Minimum value for time step adjustment"
          )

        ;

        detail::hydro_options::getOptions( hydro );
        base::hydro_stepper_options::getOptions( hydro );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "hydro",
            options_struct( hydro, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace wn_user

#endif // NNP_HYDRO_HPP
