////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define zone_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#define S_INIT_X           "init_mass_frac"

#ifndef NNP_ZONE_INIT_X_BASE_HPP
#define NNP_ZONE_INIT_X_BASE_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// zone_init_x().
//##############################################################################

class zone_init_x
{

  public:
    zone_init_x(){}
    zone_init_x( v_map_t& v_map ) : my_program_options()
    {
      if( v_map.count( S_INIT_X ) )
      {
        v_init_x =
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_INIT_X, 2
          );
      }
    }

    void
    setZoneMassFractionsFromInitX(
      nnt::Zone& zone
    )
    {

      BOOST_FOREACH(
        const std::vector<std::vector<std::string> >::value_type& v_x,
        v_init_x
      )
      {

        Libnucnet__Species * p_species =
          Libnucnet__Nuc__getSpeciesByName(
            Libnucnet__Net__getNuc(
              Libnucnet__Zone__getNet( zone.getNucnetZone() )
            ),
            v_x[0].c_str()
          );

        if( !p_species )
        {
          std::cerr <<
            v_x[0] << " not found in network." << std::endl;
          exit( EXIT_FAILURE );
        }

        Libnucnet__Zone__updateSpeciesMassFraction(
          zone.getNucnetZone(),
          p_species,
          boost::lexical_cast<double>( v_x[1] )
        );
      }

    }

  private:
    program_options my_program_options;
    std::vector<std::vector<std::string> > v_init_x;
    
};
    
//##############################################################################
// zone_init_x_options().
//##############################################################################

class zone_init_x_options
{

  public:
    zone_init_x_options(){}

    std::string
    getExample()
    {
      return
        "--" + std::string( S_INIT_X ) + " \"{h1; 0.5}\" " +
        "--" + std::string( S_INIT_X ) + " \"{n; 0.5}\" ";
    }

    void
    getOptions( po::options_description& zone_data )
    {

      try
      {

        zone_data.add_options()

        // Option for value of initial mass fraction
        (
          S_INIT_X,
          po::value<std::vector<std::string> >()->multitoken(),
          "Initial species mass fraction (enter as doublet {name; mass fraction})"
        )

        ;

// Add checks on input.

      }

      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace base

} // namespace wn_user

#endif // NNP_ZONE_INIT_X_BASE_HPP

